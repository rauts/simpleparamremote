#include "stdafx.h"
#include "HostService.h"
#include "mmcore/CoreInstance.h"
#include "vislib/math/mathfunctions.h"
#include "ProtocolSimpleParamRemote1.h"
#include "ProtocolHostInfo.h"


using namespace megamol;
using megamol::core::AbstractNamedObject;
using megamol::core::AbstractNamedObjectContainer;

unsigned int SimpleParamRemote::HostService::ID = 0;

SimpleParamRemote::HostService::HostService(core::CoreInstance& core) : AbstractService(core),
        mgAccess(core), serverThread(), serverRunning(false), address("tcp://*:35421"), commandMap() {
    // Intentionally empty
}

SimpleParamRemote::HostService::~HostService() {
    assert(!this->IsEnabled());
}

namespace {

    template <class T>
    void initAddProtocol(std::string& protocolNames, SimpleParamRemote::CommandMapType& commandMap) {
        if (!protocolNames.empty()) protocolNames += " ";
        protocolNames += T::ProtocolName();

        SimpleParamRemote::CommandMapType map = T::GetCommands();
        for (auto cmd : map) {
            std::string command = cmd.first;
            for (char& c : command) c = std::toupper(c);

            auto pos = commandMap.find(command);
            if (pos != commandMap.end()) {
                vislib::sys::Log::DefaultLog.WriteWarn("SPRHost: Command %s overwritten by %s\n", cmd.first.c_str(), T::ProtocolName());
            }
            commandMap[command] = cmd.second;
        }
    }

}

bool SimpleParamRemote::HostService::Initalize(bool& autoEnable) {
    using vislib::sys::Log;
    context = ZMQContextUser::Instance();

    // register protocols here
    commandMap["PROTOCOLS"] = &HostService::getProtocols;
    initAddProtocol<ProtocolSimpleParamRemote1>(protocols, commandMap);
    initAddProtocol<ProtocolHostInfo>(protocols, commandMap);
    // ...

    // fetch configuration
    auto& cfg = GetCoreInstance().Configuration();

    if (cfg.IsConfigValueSet("SPRHostAddress")) {
        mmcValueType t;
        const void* d = cfg.GetValue(MMC_CFGID_VARIABLE, "SPRHostAddress", &t);
        switch (t) {
        case MMC_TYPE_CSTR:
            address = static_cast<const char*>(d);
            Log::DefaultLog.WriteInfo("Set SPRHostAddress = \"%s\"", address.c_str());
            break;
        case MMC_TYPE_WSTR:
            address = vislib::StringA(static_cast<const wchar_t*>(d));
            Log::DefaultLog.WriteInfo("Set SPRHostAddress = \"%s\"", address.c_str());
            break;
        default:
            Log::DefaultLog.WriteWarn("Unable to set SPRHostAddress: expected string, but found type %d", static_cast<int>(t));
            break;
        }
    } else {
        Log::DefaultLog.WriteInfo("Default SPRHostAddress = \"%s\"", address.c_str());
    }

    autoEnable = true; // default behavior

    if (cfg.IsConfigValueSet("SPRHostEnable")) {
        mmcValueType t;
        const void* d = cfg.GetValue(MMC_CFGID_VARIABLE, "SPRHostEnable", &t);
        bool silent = false;
        switch (t) {
        case MMC_TYPE_INT32: autoEnable = ((*static_cast<const int32_t*>(d)) != 0); break;
        case MMC_TYPE_UINT32: autoEnable = ((*static_cast<const uint32_t*>(d)) != 0); break;
        case MMC_TYPE_INT64: autoEnable = ((*static_cast<const int64_t*>(d)) != 0); break;
        case MMC_TYPE_UINT64: autoEnable = ((*static_cast<const uint64_t*>(d)) != 0); break;
        case MMC_TYPE_BYTE: autoEnable = ((*static_cast<const uint8_t*>(d)) != 0); break;
        case MMC_TYPE_BOOL: autoEnable = *static_cast<const bool*>(d); break;
        case MMC_TYPE_FLOAT: autoEnable = !vislib::math::IsEqual(*static_cast<const float*>(d), 0.0f); break;
        case MMC_TYPE_CSTR:
            autoEnable = vislib::CharTraitsA::ParseBool(static_cast<const char*>(d));
            break;
        case MMC_TYPE_WSTR:
            autoEnable = vislib::CharTraitsW::ParseBool(static_cast<const wchar_t*>(d));
            break;
        default:
            Log::DefaultLog.WriteWarn("Unable to set SPRHostEnable: expected string, but found type %d", static_cast<int>(t));
            silent = true;
            break;
        }
        if (!silent) {
            Log::DefaultLog.WriteInfo("Set SPRHostEnable = \"%s\"", autoEnable ? "true" : "false");
        }
    } else {
        Log::DefaultLog.WriteInfo("Default SPRHostEnable = \"%s\"", autoEnable ? "true" : "false");
    }

    return true;
}

bool SimpleParamRemote::HostService::Deinitialize() {
    Disable();
    context.reset();
    return true;
}

void SimpleParamRemote::HostService::SetAddress(const std::string& ad) {
    if (serverRunning) {
        // restart server
        disableImpl();
        address = ad;
        enableImpl();
    } else {
        address = ad;
    }

}

bool SimpleParamRemote::HostService::enableImpl() {
    assert(serverRunning == false);
    serverThread.swap(std::thread([&](){
        this->serve();
    }));
    while (!serverRunning) std::this_thread::sleep_for(std::chrono::milliseconds(10));
    return true;
}

bool SimpleParamRemote::HostService::disableImpl() {
    serverRunning = false;
    if (serverThread.joinable()) serverThread.join();
    return true;
}

std::string SimpleParamRemote::HostService::protocols;

std::string SimpleParamRemote::HostService::getProtocols(ModuleGraphAccess& mgAccess, const std::string& param) {
    return protocols;
}

void SimpleParamRemote::HostService::serve() {
    using vislib::sys::Log;

    zmq::socket_t socket(*context, ZMQ_REP);

    try {
        serverRunning = true;
        socket.bind(address);
        socket.setsockopt(ZMQ_RCVTIMEO, 100); // message receive time out 100ms

        Log::DefaultLog.WriteInfo("SPRH Server socket opened on \"%s\"", address.c_str());

        while (serverRunning) {
            zmq::message_t request;
            while (serverRunning && !socket.recv(&request, ZMQ_DONTWAIT)) {
                // no messages available ATM
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
            }
            if (!serverRunning) break;

            std::string request_str(reinterpret_cast<char*>(request.data()), request.size());
            std::string reply = makeAnswer(request_str);
            //if (reply.empty()) reply = "ERR";
            socket.send(reply.data(), reply.size());
        }

    } catch (std::exception& error) {
        Log::DefaultLog.WriteError("Error on SPRH Server: %s", error.what());

    } catch (...) {
        Log::DefaultLog.WriteError("Error on SPRH Server: unknown exception");
    }

    try {
        socket.close();
    } catch (...) {}
    Log::DefaultLog.WriteInfo("SPRH Server socket closed");

}

std::string SimpleParamRemote::HostService::makeAnswer(const std::string& req) {

    if (req.empty()) return "ERR REQINVALID string empty";

    auto firstSpace = std::find(req.begin(), req.end(), ' ');
    std::string command;
    std::string params;
    if (firstSpace == req.end()) {
        command = req;
        params.clear();
    } else {
        command = req.substr(0, std::distance(req.begin(), firstSpace));
        params = req.substr(std::distance(req.begin(), firstSpace) + 1);
    }
    for (char& c : command) c = std::toupper(c);

    auto cmdFunc = commandMap.find(command);
    if (cmdFunc != commandMap.end()) {
        return cmdFunc->second(mgAccess, params);

    } else {
        return "ERR REQINVALID unknown request";
    }

    return "ERR generic";
}
