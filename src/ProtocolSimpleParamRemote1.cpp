#include "stdafx.h"
#include "ProtocolSimpleParamRemote1.h"
#include "vislib/sys/AutoLock.h"
#include <sstream>
#include <cstdint>
#include "mmcore/CoreInstance.h"
#include "mmcore/param/ParamSlot.h"
#include "vislib/UTF8Encoder.h"

using namespace megamol;
using megamol::core::AbstractNamedObject;
using megamol::core::AbstractNamedObjectContainer;

SimpleParamRemote::CommandMapType SimpleParamRemote::ProtocolSimpleParamRemote1::GetCommands() {
    CommandMapType rv;

    rv["QUERYPARAMS"] = &ProtocolSimpleParamRemote1::answerParamTree;
    rv["GETTYPE"] = &ProtocolSimpleParamRemote1::getParamType;
    rv["GETDESC"] = &ProtocolSimpleParamRemote1::getParamDesc;
    rv["GETVALUE"] = &ProtocolSimpleParamRemote1::getParamValue;
    rv["SETVALUE"] = &ProtocolSimpleParamRemote1::setParamValue;

    return rv;
}

std::string SimpleParamRemote::ProtocolSimpleParamRemote1::answerParamTree(ModuleGraphAccess& mgAccess, const std::string& param) {
    vislib::sys::AutoLock l(mgAccess.ModuleGraphLock());

    AbstractNamedObject::const_ptr_type ano = mgAccess.RootModule();
    AbstractNamedObjectContainer::const_ptr_type anoc = std::dynamic_pointer_cast<const AbstractNamedObjectContainer>(ano);
    if (!anoc) return "ERR QUERYBROKEN no root";

    std::stringstream answer;

    answerParamTree(answer, anoc);

    return answer.str();
}

void SimpleParamRemote::ProtocolSimpleParamRemote1::answerParamTree(std::stringstream& reply, core::AbstractNamedObjectContainer::const_ptr_type anoc) {
    if (!anoc) return;

    auto it_end = anoc->ChildList_End();
    for (auto it = anoc->ChildList_Begin(); it != it_end; ++it) {
        AbstractNamedObject::const_ptr_type ano = *it;
        const core::param::ParamSlot *param = dynamic_cast<const core::param::ParamSlot *>(ano.get());
        if (param != nullptr) {
            reply << param->FullName() << std::endl;
            continue;
        }

        AbstractNamedObjectContainer::const_ptr_type anoc = std::dynamic_pointer_cast<const AbstractNamedObjectContainer>(ano);
        if (anoc) {
            answerParamTree(reply, anoc);
        }
    }
}

std::string SimpleParamRemote::ProtocolSimpleParamRemote1::getParamType(ModuleGraphAccess& mgAccess, const std::string& param) {
    vislib::sys::AutoLock l(mgAccess.ModuleGraphLock());
    AbstractNamedObjectContainer::const_ptr_type root = std::dynamic_pointer_cast<const AbstractNamedObjectContainer>(mgAccess.RootModule());
    if (!root) return "ERR QUERYBROKEN no root";
    AbstractNamedObject::ptr_type obj = const_cast<AbstractNamedObjectContainer*>(root.get())->FindNamedObject(param.c_str());
    if (!obj) return "ERR NOTFOUND param name not found";
    core::param::ParamSlot *ps = dynamic_cast<core::param::ParamSlot*>(obj.get());
    if (ps == nullptr) return "ERR NOTFOUND name did not refer to a ParamSlot";
    auto psp = ps->Parameter();
    if (psp.IsNull()) return "ERR NOTFOUND ParamSlot does seem to hold no parameter";

    vislib::RawStorage pspdef;
    psp->Definition(pspdef);
    // not nice, but we make HEX (base64 would be better, but I don't care)
    std::string answer(pspdef.GetSize() * 2, ' ');
    for (SIZE_T i = 0; i < pspdef.GetSize(); ++i) {
        uint8_t b = *pspdef.AsAt<uint8_t>(i);
		uint8_t bh[2] = { static_cast<uint8_t>(b / 16), static_cast<uint8_t>(b % 16) };
        for (unsigned int j = 0; j < 2; ++j) answer[i * 2 + j] = (bh[j] < 10u) ? ('0' + bh[j]) : ('A' + (bh[j] - 10u));
    }

    return answer;
}

std::string SimpleParamRemote::ProtocolSimpleParamRemote1::getParamDesc(ModuleGraphAccess& mgAccess, const std::string& param) {
    vislib::sys::AutoLock l(mgAccess.ModuleGraphLock());
    AbstractNamedObjectContainer::const_ptr_type root = std::dynamic_pointer_cast<const AbstractNamedObjectContainer>(mgAccess.RootModule());
    if (!root) return "ERR QUERYBROKEN no root";
    AbstractNamedObject::ptr_type obj = const_cast<AbstractNamedObjectContainer*>(root.get())->FindNamedObject(param.c_str());
    if (!obj) return "ERR NOTFOUND param name not found";
    core::param::ParamSlot *ps = dynamic_cast<core::param::ParamSlot*>(obj.get());
    if (ps == nullptr) return "ERR NOTFOUND name did not refer to a ParamSlot";

    vislib::StringA valUTF8;
    vislib::UTF8Encoder::Encode(valUTF8, ps->Description());

    return valUTF8.PeekBuffer();
}

std::string SimpleParamRemote::ProtocolSimpleParamRemote1::getParamValue(ModuleGraphAccess& mgAccess, const std::string& param) {
    vislib::sys::AutoLock l(mgAccess.ModuleGraphLock());
    AbstractNamedObjectContainer::const_ptr_type root = std::dynamic_pointer_cast<const AbstractNamedObjectContainer>(mgAccess.RootModule());
    if (!root) return "ERR QUERYBROKEN no root";
    AbstractNamedObject::ptr_type obj = const_cast<AbstractNamedObjectContainer*>(root.get())->FindNamedObject(param.c_str());
    if (!obj) return "ERR NOTFOUND param name not found";
    core::param::ParamSlot *ps = dynamic_cast<core::param::ParamSlot*>(obj.get());
    if (ps == nullptr) return "ERR NOTFOUND name did not refer to a ParamSlot";
    auto psp = ps->Parameter();
    if (psp.IsNull()) return "ERR NOTFOUND ParamSlot does seem to hold no parameter";

    vislib::StringA valUTF8;
    vislib::UTF8Encoder::Encode(valUTF8, psp->ValueString());

    return valUTF8.PeekBuffer();
}

std::string SimpleParamRemote::ProtocolSimpleParamRemote1::setParamValue(ModuleGraphAccess& mgAccess, const std::string& param) {
    std::string name;
    std::string value;
    auto firstSpace = std::find(param.begin(), param.end(), ' ');
    if (firstSpace == param.end()) {
        name = param;
        value.clear();
    } else {
        name = param.substr(0, std::distance(param.begin(), firstSpace));
        value = param.substr(std::distance(param.begin(), firstSpace) + 1);
    }

    vislib::sys::AutoLock l(mgAccess.ModuleGraphLock());
    AbstractNamedObjectContainer::const_ptr_type root = std::dynamic_pointer_cast<const AbstractNamedObjectContainer>(mgAccess.RootModule());
    if (!root) return "ERR QUERYBROKEN no root";
    AbstractNamedObject::ptr_type obj = const_cast<AbstractNamedObjectContainer*>(root.get())->FindNamedObject(name.c_str());
    if (!obj) return "ERR NOTFOUND param name not found";
    core::param::ParamSlot *ps = dynamic_cast<core::param::ParamSlot*>(obj.get());
    if (ps == nullptr) return "ERR NOTFOUND name did not refer to a ParamSlot";
    auto psp = ps->Parameter();
    if (psp.IsNull()) return "ERR NOTFOUND ParamSlot does seem to hold no parameter";

    vislib::TString val;
    vislib::UTF8Encoder::Decode(val, value.c_str());

    return psp->ParseValue(val)
        ? "OK"
        : "ERR ParseValue failed";
}
