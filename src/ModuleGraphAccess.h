#pragma once

#include "mmcore/AbstractNamedObject.h"

namespace megamol {
namespace core {
    /** forward declaration */
    class CoreInstance;
}

namespace SimpleParamRemote {

    class ModuleGraphAccess {
    public:
        ModuleGraphAccess(core::CoreInstance& core);
        ~ModuleGraphAccess();

        core::AbstractNamedObject::const_ptr_type RootModule();
        inline vislib::sys::AbstractReaderWriterLock& ModuleGraphLock(void) {
            return RootModule()->ModuleGraphLock();
        }

    private:
        core::CoreInstance& core;
    };

}
}
