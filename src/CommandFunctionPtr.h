#pragma once

#include "ModuleGraphAccess.h"
#include <string>
#include <map>

namespace megamol {
namespace SimpleParamRemote {

    typedef std::string (*CommandFunctionPtr)(ModuleGraphAccess& mgAccess, const std::string& param);
    typedef std::map<std::string, CommandFunctionPtr> CommandMapType;

}
}
