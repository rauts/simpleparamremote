﻿using MegaMol.SimpleParamRemote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaMol.SimpleParamRemote {

    class Program {

        static void printGreeting() {
            Console.WriteLine();
            Console.WriteLine("MegaMol SimpleParamRemote ConClient");
            Console.WriteLine("Copyright 2016 by MegaMol Team, TU Dresden");
            Console.WriteLine();
        }

        static void printHelp() {
            printGreeting();

            Console.WriteLine("Syntax:");
            Console.WriteLine("\tConClient.exe [-o host [-s file [-k] ] ]");
            Console.WriteLine();
            Console.WriteLine("\t-o [host]  -  Establishes connection the specified host");
            Console.WriteLine("\t-s [file]  -  opens the specified text file and treats each line as command to be sent to the host.");
            Console.WriteLine("\t              You may only use the four commands from the last command block (see below).");
            Console.WriteLine("\t-k         -  Keeps the interactive command prompt open after the script file was executed.");
            Console.WriteLine();

            Console.WriteLine("Commands:");
            Console.WriteLine();
            Console.WriteLine("\tOPEN [host]              -  Establishes connection to the host");
            Console.WriteLine("\tCLOSE                    -  Closes connection to the host");
            Console.WriteLine("\tSTATUS                   -  Informs about the current connection");
            Console.WriteLine();
            Console.WriteLine("\tHELP                     -  Prints command line syntax and this info");
            Console.WriteLine("\tEXIT                     -  Closes this program");
            Console.WriteLine();

            Console.WriteLine("\tQUERYPARAMS              -  Answers all parameter names");
            Console.WriteLine("\tGETTYPE [name]           -  Answers the type descriptor of one parameter");
            Console.WriteLine("\tGETDESC [name]           -  Answers the human-readable description of one parameter");
            Console.WriteLine("\tGETVALUE [name]          -  Answers the value of one parameter");
            Console.WriteLine("\tSETVALUE [name] [value]  -  Sets the value of one parameter");
            Console.WriteLine("\tGETPROCESSID             -  Answers the native OS process id of the host");
            Console.WriteLine();
        }

        static void ParseCmdLine(string[] args, out string host, out string script, out bool keepOpen) {
            // ConClient.exe [-o host [-s file [-k] ] ]
            host = null;
            script = null;
            keepOpen = false;

            if (args.Length < 2) return;
            if (!args[0].Equals("-o")) {
                Console.WriteLine("Error: arg1 expected -o instead of \"" + args[0] + "\"");
                return;
            }
            host = args[1];

            if (args.Length < 4) return;
            if (!args[2].Equals("-s")) {
                Console.WriteLine("Error: arg3 expected -s instead of \"" + args[2] + "\"");
                return;
            }
            script = args[3];

            if (args.Length < 5) return;
            if (!args[4].Equals("-k")) {
                Console.WriteLine("Error: arg5 expected -k instead of \"" + args[4] + "\"");
                return;
            }
            keepOpen = true;
        }

        static private Connection con = null;

        static private string SiftFirstWord(string all, out string rem) {
            int len = all.Length;
            int i = 0;
            for (; i < len; ++i) if (!char.IsWhiteSpace(all[i])) break;
            int cmdStart = i;
            for (; i < len; ++i) if (char.IsWhiteSpace(all[i])) break;
            int cmdLen = i - cmdStart;

            for (; i < len; ++i) if (!char.IsWhiteSpace(all[i])) break;
            int remStart = i;

            rem = all.Substring(remStart);
            return all.Substring(cmdStart, cmdLen);
        }

        // known commands
        private static Request makeRequestObject(string command, string line) {
            string tail;
            switch (command) {
                case "QUERYPARAMS": return new MMSPR1.QUERYPARAMS();
                case "GETTYPE": return new MMSPR1.GETTYPE() { ParameterName = SiftFirstWord(line, out tail) };
                case "GETDESC": return new MMSPR1.GETDESC() { ParameterName = SiftFirstWord(line, out tail) };
                case "GETVALUE": return new MMSPR1.GETVALUE() { ParameterName = SiftFirstWord(line, out tail) };
                case "SETVALUE": 
                    string paramName = SiftFirstWord(line, out tail);
                    return new MMSPR1.SETVALUE() { ParameterName = paramName, ParameterValue = tail };
                case "GETPROCESSID": return new MMSPRHOSTINFO.GETPROCESSID();
            }
            return null;
        }

        static bool execCommand(string command, string line) {
            Request req = makeRequestObject(command, line);
            if (req == null) return false;

            try {
                Response reply = con.Send(req);

                if (String.IsNullOrWhiteSpace(reply.Error)) {
                    Console.Write("Reply: ");
                    switch (reply.Command) {
                        case "QUERYPARAMS":
                            Console.WriteLine();
                            foreach (string l in (string[])reply.Answer) {
                                Console.WriteLine(l);
                            }
                            break;
                        case "GETTYPE":
                            Console.WriteLine(((ParameterTypeDescription)reply.Answer).Type.ToString());
                            break;
                        default:
                            Console.WriteLine(reply.ToString());
                            break;
                    }

                } else {
                    Console.WriteLine(reply.ToString());
                }
                Console.WriteLine();


            } catch(Exception ex) {
                Console.WriteLine("ERR connection.send: " + ex.ToString());
                Console.WriteLine();
            }

            return true;
        }

        static void runScript(string file) {
            Console.WriteLine("Loading commands from \"" + file + "\"");
            foreach (string l in System.IO.File.ReadAllLines(file)) {
                string line = l;
                Console.WriteLine(line);
                string command = SiftFirstWord(line, out line).ToUpper();

                if (!execCommand(command, line)) {
                    Console.WriteLine("ERR unknown command");
                    Console.WriteLine();
                }
                System.Threading.Thread.Sleep(10);

            }

            Console.WriteLine("Script completed");
            Console.WriteLine();

        }

        static void interactiveConsole() {

            bool running = true;
            while (running) {
                Console.Write("> ");
                string line = Console.ReadLine();
                string command = SiftFirstWord(line, out line).ToUpper();

                if (command.Equals("OPEN")) {
                    //  OPEN [host]              -  Establishes connection to the host
                    if ((con != null) && con.Valid) {
                        con.Close();
                        Console.WriteLine("Socket closed");
                    }
                    try {
                        string host = SiftFirstWord(line, out line);
                        Console.Write("Connecting \"" + host + "\" ... ");
                        con = Connection.Connect(host);

                        Console.WriteLine("Connected");
                        Console.WriteLine();

                    } catch (Exception ex) {
                        Console.WriteLine();
                        Console.WriteLine("ERR Socket connection failed: " + ex.ToString());
                        Console.WriteLine();
                    }

                } else if (command.Equals("CLOSE")) {
                    //  CLOSE                    -  Closes connection to the host
                    if ((con != null) && con.Valid) {
                        con.Close();
                        con = null;
                        Console.WriteLine("Socket closed");
                    } else {
                        Console.WriteLine("Socket not connected");
                    }
                    Console.WriteLine();

                } else if (command.Equals("STATUS")) {
                    //  STATUS                   -  Informs about the current connection
                    Console.WriteLine("Socket " + (((con != null) && con.Valid) ? "connected" : "not connected"));
                    Console.WriteLine();

                } else if (command.Equals("HELP")) {
                    //  HELP                     -  Prints command line syntax and this info
                    printHelp();

                } else if (command.Equals("EXIT")) {
                    //  EXIT                     -  Closes this program
                    running = false; // leave loop

                } else {
                    if (!execCommand(command, line)) {
                        Console.WriteLine("ERR unknown command");
                        Console.WriteLine();
                    }

                }
            }

        }

        static void Main(string[] args) {
            string host;
            string script;
            bool keepOpen;

            ParseCmdLine(args, out host, out script, out keepOpen);

            if (!string.IsNullOrWhiteSpace(host)) {
                Console.Write("Connecting \"" + host + "\" ... ");
                try {
                    con = Connection.Connect(host);
                    Console.WriteLine("Connected");
                    Console.WriteLine();

                } catch (Exception ex) {
                    Console.WriteLine();
                    Console.WriteLine("ERR Socket connection failed: " + ex.ToString());
                    Console.WriteLine();
                }
            }

            if (!string.IsNullOrWhiteSpace(script)) {
                runScript(script);

            } else {
                keepOpen = true;
            }

            if (keepOpen) {
                interactiveConsole();
            }
        }

    }

}
