﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ConClient")]
[assembly: AssemblyDescription("Console Client (CSharp implementation) to the SimpleParamRemote protocol of MegaMol")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("MegaMol Team (S. Grottel)")]
[assembly: AssemblyProduct("MegaMol SimpleParamRemote")]
[assembly: AssemblyCopyright("Copyright © 2016 by MegaMol Team (S. Grottel)")]
[assembly: AssemblyTrademark("MegaMol")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8abdfd86-1fe0-48bd-83bb-38f0745a3a22")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")] // application version! This controlls, e.g., versioning for settings

[assembly: AssemblyFileVersion("1.0.0.4")] // application file version. Set build number here for minor hot fixes

[assembly: AssemblyInformationalVersionAttribute("1.2.0.0")] // product version, aka MegaMol version
