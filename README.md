# MegaMol plugin SimpleParamRemote
This plugin provides simple remote control over parameters. Inter-process communication is based on TCP using ZeroMQ. The plugin is accompanied by two console clients (mainly meant for debugging), one written in c++, one written in c#, and a managed library for clients (written in c#) to be used in further projects.

The plugin and all additional projects in this repository are publicly available under terms of the [BSD license](https://bitbucket.org/MegaMolDev/simpleparamremote/raw/3575b49f2e982a15988aa1a88c2dddc6cf3c4a0c/LICENSE.txt).

## The Plugin
The plugin the primary element of this repository and is located in the usual directories for MegaMol plugin projects:

* The root directory,

* include, and

* src

## ConClient
The subdirectory “ConClient” holds a console client implementation in C++. This client is mainly meant for debugging, but can be used to script MegaMol. Online help is available:

* Start “ConClient.exe”; then

* Enter “help” in the console command prompt.

## ConClient.Net
The subdirectory “ConClient.Net” holds a second console client implemented in C#. This is basically a port of the C++ “ConClient”, meant to debug the managed client library. It is almost equal in features to the C++ client, but should not be used in productive use, as functionality is not guaranteed.

## ClientLib.Net
The subdirectory “ClientLib.Net” holds the managed client library. This library is used by MegaMolConfigurator and MegaMolMovieMaker to control instances of MegaMol.

Official releases of this library are available via [Nuget: https://www.nuget.org/packages/MegaMol.SimpleParamRemote.ClientNet]( https://www.nuget.org/packages/MegaMol.SimpleParamRemote.ClientNet)