﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaMol.SimpleParamRemote {

    /// <summary>
    /// Abstract implementation helper for generic requests
    /// </summary>
    public abstract class AbstarctGenericRequest : Request {
        protected string cmd;
        protected string[] param;

        /// <summary>
        /// The command string to be sent
        /// </summary>
        public override string Command { get { return cmd; } }

        /// <summary>
        /// The parameters of the command
        /// </summary>
        protected override string[] parameters { get { return param; } }
    }

    /// <summary>
    /// An generic request allowing to send anything
    /// </summary>
    public class GenericRequest : AbstarctGenericRequest {

        /// <summary>
        /// Gets or sets the command string to be sent
        /// </summary>
        public new string Command { get { return cmd; } set { cmd = value; } }

        /// <summary>
        /// Gets or sets the parameters of the command
        /// </summary>
        public string[] Parameters { get { return param; } set { param = value; } }

    }

}
