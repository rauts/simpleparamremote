﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SimpleParamRemote Client")]
[assembly: AssemblyDescription("Client end point library for the SimpleParamRemote protocol of MegaMol")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("MegaMol Team (S. Grottel)")]
[assembly: AssemblyProduct("MegaMol SimpleParamRemote")]
[assembly: AssemblyCopyright("Copyright © 2016 by MegaMol Team (S. Grottel)")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("2789007c-bae1-4748-be2f-4dcb2a4b0808")]

[assembly: AssemblyVersion("1.0.0.0")] // plugin version! This controlls, e.g., versioning for settings

[assembly: AssemblyFileVersion("1.0.0.3")] // plugin file version. Set build number here for minor hot fixes

[assembly: AssemblyInformationalVersionAttribute("1.2.0.0")] // product version, aka MegaMol version
