﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZeroMQ;

namespace MegaMol.SimpleParamRemote {

    /// <summary>
    /// A request to be send to MegaMol SPR Host
    /// </summary>
    public abstract class Request {

        /// <summary>
        /// The command string to be sent
        /// </summary>
        public abstract string Command { get; }

        /// <summary>
        /// The parameters of the command
        /// </summary>
        protected abstract string[] parameters { get; }

        internal ZFrame MakeZMQRequest() {
            string s = Command.ToString();

            if (parameters != null) {
                s += " " + string.Join(" ", parameters);
            }

            return new ZFrame(s, Encoding.UTF8);
        }

        internal virtual object parseAnswerFromZString(string v) {
            return v;
        }

    }

}
