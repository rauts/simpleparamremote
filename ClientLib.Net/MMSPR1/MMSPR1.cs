﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaMol.SimpleParamRemote.MMSPR1 {

    /// <summary>
    /// Answers all parameter names
    /// </summary>
    public sealed class QUERYPARAMS : Request {
        public override string Command { get { return "QUERYPARAMS"; } }
        protected override string[] parameters { get { return null; } }
        internal override object parseAnswerFromZString(string v) {
            return v.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }

    /// <summary>
    /// Answers the type descriptor of one parameter
    /// </summary>
    public sealed class GETTYPE : Request {
        public override string Command { get { return "GETTYPE"; } }
        /// <summary>
        /// Gets or sets the name of the parameter slot to query
        /// </summary>
        public string ParameterName { get; set; }
        protected override string[] parameters { get { return new string[] { ParameterName }; } }

        /// <remarks>
        /// http://stackoverflow.com/a/321404/552373
        /// </remarks>
        private static byte[] StringToByteArray(string hex) {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        internal override object parseAnswerFromZString(string v) {
            byte[] dat = StringToByteArray(v);
            ParameterTypeDescription d = new ParameterTypeDescription();
            d.SetFromBinDescription(dat);
            return d;
        }
    }

    /// <summary>
    /// Answers the human-readable description of one parameter
    /// </summary>
    public sealed class GETDESC : Request {
        public override string Command { get { return "GETDESC"; } }
        /// <summary>
        /// Gets or sets the name of the parameter slot to query
        /// </summary>
        public string ParameterName { get; set; }
        protected override string[] parameters { get { return new string[] { ParameterName }; } }
    }

    /// <summary>
    /// Answers the value of one parameter
    /// </summary>
    public sealed class GETVALUE : Request {
        public override string Command { get { return "GETVALUE"; } }
        /// <summary>
        /// Gets or sets the name of the parameter slot to query
        /// </summary>
        public string ParameterName { get; set; }
        protected override string[] parameters { get { return new string[] { ParameterName }; } }
    }

    /// <summary>
    /// Sets the value of one parameter
    /// </summary>
    public sealed class SETVALUE : Request {
        public override string Command { get { return "SETVALUE"; } }
        /// <summary>
        /// Gets or sets the name of the parameter slot to set
        /// </summary>
        public string ParameterName { get; set; }
        /// <summary>
        /// Gets or sets the new value for the parameter
        /// </summary>
        public string ParameterValue { get; set; }
        protected override string[] parameters { get { return new string[] { ParameterName, ParameterValue }; } }
        internal override object parseAnswerFromZString(string v) {
            return true; // if there was an error, the checks before will have received it.
        }
    }

}
